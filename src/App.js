import React, { useState } from 'react';
import axios from 'axios';
import videoSrc from './images/bg.mp4';
import './App.css';




function App() {

  const [url, setUrl] = useState('');
  const [generatedUrl, setGeneratedUrl] = useState('');

  const getUrl = (x) => {
    axios.post('https://rel.ink/api/links/', {url: x}).then((r) => {
      setGeneratedUrl('https://rel.ink/' + r.data.hashid);
    })
  }
  
 const copyUrl = () => {
  var el = document.createElement('textarea');
  el.value = generatedUrl;
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  alert('Link is copied');
 }
  
  return (
    <div className="App">
      <video className="videoBg" muted autoPlay loop>
        <source src={videoSrc} type="video/mp4"></source>
      </video>
      <div className="mainDiv">
        <h1 className="mainHeader">Url shortener</h1>
        <p className="mainParagraph">Secure and reliable short links.</p>
        <div className="mainInput">
          <input id="inputField" placeholder="Enter url here" type="text" onChange={e => setUrl(e.target.value)}></input>
          <button className="mainButton" onClick={() => getUrl(url)}>Get url</button>
        </div>
        <div className="generatedUrl" onClick={copyUrl}>{generatedUrl}</div>
      </div>
    </div>
  );
}

export default App;
